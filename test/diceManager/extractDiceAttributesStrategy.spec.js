const faker = require('faker')
const assert = require('chai').assert

const diceShortAttributesFixture = require('../fixtures/diceShortAttributes.js')

const extractDiceAttributesStrategy = require('../../src/diceManager/extractDiceAttributesStrategy.js')

describe('#extractDiceAttributesStrategy', () => {
  describe('Default behavior', () => {

    const expectedResult = {
      faces: 20,
      times: 1,
      modifier: '+0',
      shortAttributes: 'd20'
    }


    it('should return the proper object', () => {
      const diceAttributes = extractDiceAttributesStrategy()
      assert.deepEqual(diceAttributes, expectedResult)
    })


    it('should return the default object when invalid attributes are passed', () => {
      const invalidAttributes = [
        faker.random.number(),
        faker.random.word(),
        faker.internet.email(),
        faker.internet.url()
      ]

      invalidAttributes.forEach((value) => {
        const result = extractDiceAttributesStrategy(value)
        assert.isUndefined(result)
      })
    })
  })
})
