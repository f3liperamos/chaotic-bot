const faker = require('faker')
const assert = require('chai').assert

const diceAttributesFixture = require('../fixtures/diceAttributes.js')
const diceManager = require('../../src/diceManager/diceManager.js')

describe('#diceManager', () => {
  describe('Default behavior', () => {
    const diceValue = diceManager()

    it('should return an array with a single value when no argument is passed', () => {
      assert.lengthOf(diceValue.values, 1)
    })

    it('should return a value at most 20 (default value)', () => {
      diceValue.values.forEach(roll => assert.isAtMost(roll, 20))
    })

    it('should return maxValue properly (default 20)', () => {
      assert.equal(diceValue.maxValue, 20)
    })
  })

  describe('Random Parameters', () => {
    const diceAttributes = diceAttributesFixture()
    const diceRolls = diceManager(diceAttributes)

    it('should return as many values as given', () => {
      assert.lengthOf(diceRolls.values, diceAttributes.times)
    })

    it('it should not exceed the maximum value per dice', () => {
      diceRolls.values.forEach(roll => assert.isAtMost(roll, diceAttributes.faces))
    })
  })

  describe('Fixed parameters', () => {
    const diceAttributes = {
      times: 10,
      faces: 1,
      modifier: '+2',
      shortAttributes: '10d1+2'
    }

    const diceRolls = diceManager(diceAttributes)
    const maxValue = 12
    const total = 12
    const friendlyTotal = '10 +2 = 12'

    it('should return maxValue properly', () => {
      assert.equal(diceRolls.maxValue, maxValue)
    })

    it('should return total properly', () => {
      assert.equal(diceRolls.total, total)
    })

    it('should return friendlyTotal properly', () => {
      assert.equal(diceRolls.friendlyTotal, friendlyTotal)
    })
  })
})
