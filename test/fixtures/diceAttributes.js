const faker = require('faker')

const defaultValues = {
  modifier: faker.random.arrayElement(['+', '-']),
  maxValue: 1000
}

module.exports = ({ modifier, maxValue } = defaultValues) => {
  const diceAttributes = {
    faces: faker.random.number(maxValue),
    times: faker.random.number(maxValue),
    modifier: modifier + faker.random.number(maxValue)
  }

  diceAttributes.shortAttributes = `${diceAttributes.times}d${diceAttributes.faces}${diceAttributes.modifier}`

  return diceAttributes
}
