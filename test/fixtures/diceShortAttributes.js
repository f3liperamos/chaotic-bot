const faker = require('faker');

module.exports = () => {
  const modifier = faker.random.arrayElement(['+', '-']) + faker.random.number();
  return `${faker.random.number()}d${faker.random.number()}${modifier}`;
}
