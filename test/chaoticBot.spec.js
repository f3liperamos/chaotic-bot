const faker = require('faker')
const assert = require('chai').assert

const chaoticBot = require('../src/chaoticBot.js')

describe('#ChaoticBot', () => {
  describe('Basic Bot Behavior', () => {
    it('should return an error message when action doesn\'t exist',() => {
      const actionName = faker.lorem.word()
      const result = chaoticBot.callAction(actionName)

      assert.equal(result, chaoticBot.SORRY_MESSAGE)
    })

    it('should return something when action exist',() => {
      const actionName = faker.lorem.word()

      const action = parameters => assert.isUndefined(parameters)
      action.match = parameters => {
        assert.equal(parameters, actionName)
        return true
      }

      chaoticBot.register(action)

      const result = chaoticBot.callAction({ name: actionName })

      assert.notEqual(result, chaoticBot.SORRY_MESSAGE)
    })
  })
})
