const diceManager = require('../diceManager/diceManager');
const extractDiceAttributesStrategy = require('../diceManager/extractDiceAttributesStrategy');

const rollAction = (parameters) => {
  const diceAttributes = extractDiceAttributesStrategy(parameters)
  return diceManager(diceAttributes)
}

rollAction.match = action => /^roll$/.test(action)

module.exports = rollAction;
