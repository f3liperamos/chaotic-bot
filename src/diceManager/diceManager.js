const lodash = require('lodash')

const diceManager = ({ faces, times, modifier, shortAttributes } = diceManager.DEFAULT_PARAMS, dependencies = { lodash }) => {
  const diceRolls = {
    values: dependencies.lodash.times(times, () => Math.floor(Math.random() * faces) + 1)
  }

  const modifierValue = parseInt(modifier)
  const diceRollsTotal = diceRolls.values.reduce((reducedValue, nextValue) => reducedValue + nextValue, 0)

  diceRolls.maxValue = (times * faces) + modifierValue
  diceRolls.total = diceRollsTotal + modifierValue
  diceRolls.friendlyTotal = `${diceRollsTotal} ${modifier} = ${diceRolls.total}`
  diceRolls.shortAttributes = shortAttributes

  return diceRolls
}

diceManager.DEFAULT_PARAMS = {
  faces: 20,
  times: 1,
  modifier: '+0',
  shortAttributes: 'd20'
}

module.exports = diceManager
