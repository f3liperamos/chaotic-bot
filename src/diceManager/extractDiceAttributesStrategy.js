const extractDiceAttributesStrategy = (diceShortAttributes = extractDiceAttributesStrategy.DEFAULT_ATTRIBUTES) => {

  const extractParameters = extractDiceAttributesStrategy.REG_EXP.exec(diceShortAttributes);

  if (extractParameters) {
    return {
      times: parseInt(extractParameters[1]) || 1,
      faces: parseInt(extractParameters[2]),
      modifier: extractParameters[3] || '+0',
      shortAttributes: diceShortAttributes
    }
  }
};

extractDiceAttributesStrategy.REG_EXP = /^(\d*)d(\d+)([+-]\d+)?/;
extractDiceAttributesStrategy.DEFAULT_ATTRIBUTES = 'd20';

module.exports = extractDiceAttributesStrategy;
