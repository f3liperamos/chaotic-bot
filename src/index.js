const ChaoticBot = require('./chaoticBot')
const actionsPath = require("path").join(__dirname, "actions");

require("fs").readdirSync(actionsPath).forEach(function(file) {
  ChaoticBot.register(require("./actions/" + file))
});

module.exports = ChaoticBot;
