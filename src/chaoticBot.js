const registered = []

const chaoticBot = {
  callAction(action) {
    const strategy = chaoticBot.find(action.name)

    if (strategy) {
      return strategy(action.parameters)
    }

    return chaoticBot.SORRY_MESSAGE
  },

  find(actionName) {
    return registered.find(actionStrategy => actionStrategy.match(actionName))
  },

  register(strategy) {
    registered.push(strategy)
  }
}

chaoticBot.SORRY_MESSAGE = 'What?!'

module.exports = chaoticBot
